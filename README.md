﻿# HikvisionAPI
This is a small console application that allows you to change the 
camera view to a selected preset. Presets are predefined camera 
positions that can be programmed using the web interface of the 
Hikvision camera. When the presets are 
programmed, you can use this tool to let the camera change to 
one of the presets. It is ideal in combination with for example a
[Stream Deck](https://www.elgato.com/en/gaming/stream-deck) and [OBS](https://obsproject.com/).

![Cm70](https://data.kommago.nl/img/products/normal/hikvision-ds-2cd2347g1-lu.jpg)

This tool is written in .NET Core so it can be used on Windows, Mac and Linux platforms.

## Download
* [HikvisionAPI for Windows](https://tonsnoei.nl/wp-content/uploads/2020/07/AvonicChangePreset_windows.zip)
* [HikvisionAPI for Linux](https://tonsnoei.nl/wp-content/uploads/2020/07/AvonicChangePreset.tar.gz)
* [HikvisionAPI for Mac](https://tonsnoei.nl/wp-content/uploads/2020/07/AvonicChangePreset_mac.zip)

Use it as follows at the command line of your OS:
```bash
/my_binaries/hikvisionapi.exe <ip> <username> <password> <preset>

# Example below sets the camera with 
# ip 10.0.0.3 and username: admin and password: admin 
# to the camera position of preset 0
/my_binaries/hikvisionapi.exe 10.0.0.3 admin admin 0
```
The `mybinaries` path is an example. Point it to the path where you installed the tool.

## With Stream Deck
If you want to use it in combination with a Stream Deck. Create a Windows batch file or 
Linux bash file first with the command as shown above and call that from the Stream Deck `Open` command.

##### Batch file: to_preset_1.bat
```
"c:\avonicchangepreset\hikvisionapi.exe" 10.0.0.3 admin admin 1
```



Have Fun!