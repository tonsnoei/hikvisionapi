﻿using System;
using System.Net;

namespace hikvisionapi
{
    class Program
    {
        static void Main(string[] args)
        {
            // Parameters
            // - ip
            // - username
            // - password
            // - preset

            if (args.Length != 4)
            {
                Console.WriteLine("Usage: hikvisionapi <ip> <username> <password> <preset>");
                Console.WriteLine("Sampl: hikvisionapi 192.168.1.1 user pwd 1");
            }

            string ip = args[0];
            string username = args[1];
            string password = args[2];
            string preset = args[3];

            WebClient client = new WebClient();
            client.Credentials = new NetworkCredential(username, password);
            string result = client.UploadString($"http://{ip}/ISAPI/PTZCtrl/channels/1/presets/{preset}/goto", WebRequestMethods.Http.Put, "");
            Console.WriteLine(result);
        }
    }
}
